package net.tyw.core.auth;


import net.tyw.common.encry.Encrypt;

import java.util.Set;

public abstract class OauthService {
    public OauthService() {
    }

    public String genPwd(String loginPwd, String secret, boolean needSha1) {
        if (loginPwd == null) {
            return "";
        } else {
            if (needSha1) {
                loginPwd = Encrypt.sha1(loginPwd);
            }

            secret = Encrypt.md5(secret.toUpperCase());
            return Encrypt.md5(loginPwd.toUpperCase() + "@" + secret);
        }
    }


    public abstract void setAuth(String sessionId, OauthUser oauthUser);

    public abstract OauthUser getOAuth(String sessionId);

    public abstract void destroy(String sessionId);

    public abstract void setAuthorities(String sessionId, Set<String> listAuthorities);

    public abstract boolean hasAuthority(String sessionId, String authority);

    public abstract Set<String> getAuthorities(String sessionId);

}
