package net.tyw.core.auth;

import java.io.Serializable;
import java.util.Date;

/**
 * @author jtoms
 */
public class OauthUser implements Serializable {

    private static final long serialVersionUID = -7252878048497511413L;

    /**
     * 唯一标识
     */
    private Long id;

    /**
     * 用户的登录账号
     */
    private String loginName;

    /**
     * 最后登录时间
     */
    private Date lastLoginTime;

    /**
     * 用户登录令牌
     */
    private String sessionId;

    public OauthUser() {
        super();
    }

    public static OauthUser instance(Long id, String loginName, Date lastLoginTime, String sessionId) {
        OauthUser oauthUser = new OauthUser();
        oauthUser.setId(id).setLoginName(loginName).setLastLoginTime(lastLoginTime).setSessionId(sessionId);
        return oauthUser;
    }

    public Long getId() {
        return id;
    }

    public OauthUser setId(Long id) {
        this.id = id;
        return this;
    }


    public String getLoginName() {
        return loginName;
    }

    public OauthUser setLoginName(String loginName) {
        this.loginName = loginName;
        return this;
    }

    public String getSessionId() {
        return sessionId;
    }

    public OauthUser setSessionId(String sessionId) {
        this.sessionId = sessionId;
        return this;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public OauthUser setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
        return this;
    }
}
