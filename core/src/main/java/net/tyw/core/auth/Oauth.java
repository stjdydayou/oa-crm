package net.tyw.core.auth;


import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Oauth {
    @AliasFor("authority")
    String[] value() default {};

    @AliasFor("value")
    String[] authority() default {};
}
