package net.tyw.core.mapper.oauth;

import net.tyw.common.paginator.domain.PageBounds;
import net.tyw.common.paginator.domain.PageList;
import net.tyw.core.enums.oauth.UserLoginAccountType;
import net.tyw.core.enums.oauth.UserPasswdType;
import net.tyw.core.model.oauth.UserInfo;
import net.tyw.core.model.oauth.UserLoginAccount;
import net.tyw.core.model.oauth.UserLoginLog;
import net.tyw.core.model.oauth.UserPassword;
import net.tyw.core.vo.oauth.UserInfoVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author jtoms
 * @date 2017-6-7
 */
public interface UserInfoMapper {

    /**
     * 分页查询用户列表
     *
     * @param bounds
     * @param vo
     * @return
     */
    PageList<UserInfo> findPage(PageBounds bounds, UserInfoVo vo);

    /**
     * 通过账号与账号类型查询用户的登录账号
     *
     * @param loginAccount
     * @param accountType
     * @return
     */
    UserLoginAccount findLoginAccount(@Param("loginAccount") String loginAccount, @Param("accountType") UserLoginAccountType accountType);

    /**
     * 查询用户下所的登录账号
     *
     * @param uid
     * @return
     */
    List<UserLoginAccount> findAllLoginAccount(@Param("uid") Long uid);

    /**
     * 查询用户基本信息
     *
     * @param id
     * @return
     */
    UserInfo findUserInfoById(@Param("id") Long id);


    /**
     * 查询用户登录密码
     *
     * @param uid
     * @param userPasswdType
     * @return
     */
    UserPassword findUserPassword(@Param("uid") Long uid, @Param("userPasswdType") UserPasswdType userPasswdType);

    /**
     * 查询最后一次登录
     *
     * @param uid
     * @return
     */
    UserLoginLog findLastLogin(Long uid);

    /**
     * 插入登录日志
     *
     * @param loginLog
     */
    void insertLoginLog(UserLoginLog loginLog);


    /**
     * 插入一条密码记录
     *
     * @param userPassword
     */
    void insertPassword(UserPassword userPassword);

    /**
     * 更新密码
     *
     * @param uid
     * @param passwd
     * @param salt
     * @param userPasswdType
     */
    void updateUserPassword(@Param("uid") Long uid, @Param("passwd") String passwd, @Param("salt") String salt, @Param("userPasswdType") UserPasswdType userPasswdType);


    /**
     * 插入一个用户基本信息
     *
     * @param userInfo
     */
    void insertUserInfo(UserInfo userInfo);

    /**
     * @param uid
     * @param loginAccount
     * @param accountType
     */
    void insertLoginAccount(@Param("uid") Long uid, @Param("loginAccount") String loginAccount, @Param("accountType") UserLoginAccountType accountType);

    /**
     * 更新用户基本信息
     *
     * @param userInfo
     */
    void updateUserInfo(UserInfo userInfo);

    /**
     * 删除用户角色
     *
     * @param uid
     */
    void deleteUserRole(@Param("uid") Long uid);

    /**
     * 插入用户角色
     *
     * @param uid
     * @param listRoleIds
     */
    void insertUserRole(@Param("uid") Long uid, @Param("listRoleIds") List<Long> listRoleIds);

}
