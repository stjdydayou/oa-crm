package net.tyw.core.mapper.oauth;

import net.tyw.core.model.oauth.RoleInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author jtoms
 * @date 2017-6-7
 */
public interface RoleMapper {

    /**
     * 查询所有用户角色
     *
     * @return
     */
    List<RoleInfo> findAllRole();

    /**
     * 查询所有用户角色
     *
     * @param uid
     * @return
     */
    List<RoleInfo> findRoleByUser(@Param("uid") Long uid);

    /**
     * 查询一个角色信息
     *
     * @param roleId
     * @return
     */
    RoleInfo findById(@Param("roleId") Long roleId);

    /**
     * 添加一个新的角色
     *
     * @param roleInfo
     */
    void insert(RoleInfo roleInfo);

    /**
     * 更新一个角色
     *
     * @param roleInfo
     */
    void update(RoleInfo roleInfo);

    /**
     * 删除角色
     *
     * @param listRoleIds
     */
    void delete(@Param("listRoleIds") List<Long> listRoleIds);

    /**
     * 删除角色下的权限资源
     *
     * @param listRoleIds
     */
    void deleteRoleResource(@Param("listRoleIds") List<Long> listRoleIds);

    /**
     * 向角色插入权限资源
     *
     * @param roleId
     * @param listResourceIds
     */
    void insertRoleResource(@Param("roleId") Long roleId, @Param("listResourceIds") List<Long> listResourceIds);
}
