package net.tyw.core.mapper.oauth;

import net.tyw.core.model.oauth.Resource;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author jtoms
 * @date 2017-6-7
 */
public interface ResourceMapper {

    /**
     * 加载所有的权限资源
     *
     * @param parentId
     * @return
     */
    List<Resource> findAllResource(@Param("parentId") Long parentId);


    /**
     * 查询角色下的权限资源
     *
     * @param roleId
     * @return
     */
    List<Resource> findResourceByRole(@Param("roleId") Long roleId);

    /**
     * 查询角色下的权限资源
     *
     * @param uid
     * @return
     */
    List<Resource> findResourceByUser(@Param("uid") Long uid);
}
