package net.tyw.core.enums.oauth;

import com.alibaba.fastjson.serializer.ObjectSerializer;

public enum UserState {

    normal(1, "正常"), disable(0, "禁用");

    private int code;
    private String text;

    UserState(int code, String text) {
        this.code = new Integer(code);
        this.text = text;
    }

    public static UserState valueOf(int code) {
        UserState[] values = UserState.values();
        for (UserState type : values) {
            if (type.code == code) {
                return type;
            }
        }
        return null;
    }

    public static ObjectSerializer jsonSerializer() {
        return (jsonSerializer, object, fieldName, fieldType, features) -> {
            UserState obj = (UserState) object;
            jsonSerializer.out.write("{ \"code\": " + obj.getCode() + ",\"text\": \"" + obj.getText() + "\"}");
        };
    }

    public int getCode() {
        return code;
    }

    public String getText() {
        return text;
    }

}
