package net.tyw.core.enums.oauth;

import com.alibaba.fastjson.serializer.ObjectSerializer;

public enum Gender {
    secret(0, "保密"), male(1, "男"), female(2, "女");

    private int code;
    private String text;

    Gender(int code, String text) {
        this.code = code;
        this.text = text;
    }

    public static Gender valueOf(int code) {
        Gender[] values = Gender.values();
        for (Gender type : values) {
            if (type.code == code) {
                return type;
            }
        }
        return null;
    }

    public static ObjectSerializer jsonSerializer() {
        return (jsonSerializer, object, fieldName, fieldType, features) -> {
            Gender obj = (Gender) object;
            jsonSerializer.out.write("{ \"code\": " + obj.getCode() + ",\"text\": \"" + obj.getText() + "\"}");
        };
    }

    public int getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
