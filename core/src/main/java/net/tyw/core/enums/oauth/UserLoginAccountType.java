package net.tyw.core.enums.oauth;

/**
 * @author jtoms
 */

public enum UserLoginAccountType {
    email(1, "邮箱"),
    mp(2, "手机号");

    private int code;
    private String text;

    UserLoginAccountType(int code, String text) {
        this.code = code;
        this.text = text;
    }

    public static UserLoginAccountType valueOf(int code) {
        UserLoginAccountType[] values = UserLoginAccountType.values();
        for (UserLoginAccountType type : values) {
            if (type.code == code) {
                return type;
            }
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
