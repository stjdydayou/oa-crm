package net.tyw.core.enums.oauth;

public enum UserPasswdType {
    login(1, "登录密码"), trade(2, "交易密码");

    private int code;
    private String text;

    UserPasswdType(int code, String text) {
        this.code = code;
        this.text = text;
    }

    public static UserPasswdType valueOf(int code) {
        UserPasswdType[] values = UserPasswdType.values();
        for (UserPasswdType type : values) {
            if (type.code == code) {
                return type;
            }
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public String getText() {
        return text;
    }
}
