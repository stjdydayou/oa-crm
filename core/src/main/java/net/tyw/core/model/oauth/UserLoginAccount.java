package net.tyw.core.model.oauth;

import net.tyw.core.enums.oauth.UserLoginAccountType;

import java.io.Serializable;

/**
 * @author jtoms
 * @date 2017-6-7
 */
public class UserLoginAccount implements Serializable {

    private static final long serialVersionUID = -6476815660455154143L;

    private Long id;

    private Long uid;

    private String loginAccount;

    private UserLoginAccountType accountType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getLoginAccount() {
        return loginAccount;
    }

    public void setLoginAccount(String loginAccount) {
        this.loginAccount = loginAccount;
    }

    public UserLoginAccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(UserLoginAccountType accountType) {
        this.accountType = accountType;
    }
}
