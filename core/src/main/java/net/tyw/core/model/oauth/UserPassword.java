package net.tyw.core.model.oauth;


import net.tyw.core.enums.oauth.UserPasswdType;

import java.io.Serializable;

/**
 * @author jtoms
 */
public class UserPassword implements Serializable {

    private static final long serialVersionUID = 5320982429919793484L;

    private Long id;

    private Long uid;

    private String passwd;

    private String salt;

    private UserPasswdType type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd == null ? null : passwd.trim();
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt == null ? null : salt.trim();
    }

    public UserPasswdType getType() {
        return type;
    }

    public void setType(UserPasswdType type) {
        this.type = type;
    }
}