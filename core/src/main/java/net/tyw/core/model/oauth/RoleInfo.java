package net.tyw.core.model.oauth;

import java.io.Serializable;

public class RoleInfo implements Serializable {

    private static final long serialVersionUID = 5241138963805332125L;

    private Long id;

    private String name;

    private String remark;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}