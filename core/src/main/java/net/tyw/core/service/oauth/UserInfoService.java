package net.tyw.core.service.oauth;

import net.tyw.common.exception.ServiceException;
import net.tyw.common.paginator.domain.PageBounds;
import net.tyw.common.paginator.domain.PageResult;
import net.tyw.core.enums.oauth.UserLoginAccountType;
import net.tyw.core.enums.oauth.UserPasswdType;
import net.tyw.core.model.oauth.*;
import net.tyw.core.vo.oauth.UserInfoVo;

import java.util.List;

/**
 * @author jtoms
 */
public interface UserInfoService {

    /**
     * 注册一个新的用户
     *
     * @param userInfo
     * @param email
     * @param mp
     * @param loginPassword
     * @param roles
     * @return
     * @throws ServiceException
     */
    void register(UserInfo userInfo, String email, String mp, String loginPassword, List<Long> roles) throws ServiceException;

    /**
     * 查询用户单个的登录账号
     *
     * @param loginAccount
     * @param accountType
     * @return
     */
    UserLoginAccount findLoginAccount(String loginAccount, UserLoginAccountType accountType);

    /**
     * 查询用户下所的登录账号
     *
     * @param uid
     * @return
     */
    List<UserLoginAccount> findAllLoginAccount(Long uid);

    /**
     * 查询用户基本信息
     *
     * @param id
     * @return
     */
    UserInfo findUserInfoById(Long id);

    /**
     * 查询用户登录密码
     *
     * @param uid
     * @param userPasswdType
     * @return
     */
    UserPassword findUserPassword(Long uid, UserPasswdType userPasswdType);

    /**
     * 查询最后一次登录
     *
     * @param uid
     * @return
     */
    UserLoginLog findLastLogin(Long uid);

    /**
     * 更新用户登录信息
     *
     * @param uid
     * @param remoteIp
     */
    void updateLoginInfo(Long uid, String remoteIp);

    /**
     * 更新密码
     *
     * @param uid
     * @param passwd
     * @param salt
     * @param userPasswdType
     */
    void updateUserPassword(Long uid, String passwd, String salt, UserPasswdType userPasswdType);

    /**
     * 分页查询用户列表
     *
     * @param bounds
     * @param vo
     * @return
     */
    PageResult<UserInfo> findPage(PageBounds bounds, UserInfoVo vo);

    /**
     * 更新用户基本信息
     *
     * @param userInfo
     * @param loginPassword
     */
    void updateUserInfo(UserInfo userInfo, String loginPassword, List<Long> roles) throws ServiceException;


    /**
     * 加载所有的权限资源
     *
     * @param parentId
     * @return
     */
    List<Resource> findAllResource(Long parentId);

    /**
     * 查询角色下的权限资源
     *
     * @param roleId
     * @return
     */
    List<Resource> findResourceByRole(Long roleId);

    /**
     * 查询角色下的权限资源
     *
     * @param uid
     * @return
     */
    List<Resource> findResourceByUser(Long uid);

    /**
     * 查询所有用户角色
     *
     * @return
     */
    List<RoleInfo> findAllRole();

    /**
     * 查询所有用户角色
     *
     * @return
     */
    List<RoleInfo> findRoleByUser(Long uid);

    /**
     * 查询一个角色信息
     *
     * @param roleId
     * @return
     */
    RoleInfo findRoleById(Long roleId);

    /**
     * 保存用户角色
     *
     * @param roleInfo
     * @param listResourceIds
     * @throws ServiceException
     */
    void saveRole(RoleInfo roleInfo, List<Long> listResourceIds) throws ServiceException;

    /**
     * 删除角色
     *
     * @param listRoleIds
     * @throws ServiceException
     */
    void deleteRole(List<Long> listRoleIds) throws ServiceException;
}
