/**
 *
 */
package net.tyw.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * @version V1.0
 * @Title: BaseService.java
 * @Description: Service 基础类，提供基本Service支持
 */

public class BaseService extends ApplicationObjectSupport {

    @Autowired
    protected TransactionTemplate transactionTemplate;

    public TransactionTemplate getTransactionTemplate() {
        return transactionTemplate;
    }

}
