import axios from 'axios';

import {API_HOSH} from '@/CONSTANTS';

export const callback = axiosPromise => {

};

export const requestLogin = params => {
    return axios.post(`${API_HOSH}/login`, params);
};