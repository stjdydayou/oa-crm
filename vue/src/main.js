// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css';
import '@/styles/common.css';
import VueRouter from 'vue-router'

import routes from './routes'
import auth from './mixins/auth'
import http from './mixins/http'
import {AUTH_CONSTANTS} from '@/CONSTANTS';


Vue.use(ElementUI);
Vue.use(VueRouter);

Vue.mixin({
    mixins: [auth, http]
});

Vue.config.productionTip = false;

const router = new VueRouter({
    routes
});

router.beforeEach((to, from, next) => {

    let authString = sessionStorage.getItem(AUTH_CONSTANTS.auth);
    let oauthUser = JSON.parse(authString);
    let isAuth = (oauthUser !== null && oauthUser !== undefined && oauthUser.id > 0);

    if (to.meta.requireAuth) {  // 判断该路由是否需要登录权限
        if (isAuth) {
            next();
        } else {
            sessionStorage.removeItem(AUTH_CONSTANTS.auth);
            localStorage.removeItem(AUTH_CONSTANTS.autoLogin);
            router.push('/login');
        }
    } else {
        next();
    }
});

new Vue({
    router, render: h => h(App)
}).$mount('#app');

router.push('/welcome');
