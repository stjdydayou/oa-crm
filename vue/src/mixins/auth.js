import {AUTH_CONSTANTS} from '@/CONSTANTS';

export default {
    data() {
        return {}
    },
    computed: {},
    mounted: function () {

    },
    methods: {
        oauth: function (oauthResource) {
            let authoritiesString = sessionStorage.getItem(AUTH_CONSTANTS.authorities);
            if (authoritiesString === undefined || authoritiesString === null) {
                return false;
            }
            let result = false;
            if (typeof(oauthResource) === 'string') {
                result = authoritiesString.indexOf('"' + oauthResource + '"') > -1
            }
            if (typeof(oauthResource) === 'object') {
                for (let i = 0; i < oauthResource.length; i++) {
                    if (authoritiesString.indexOf('"' + oauthResource[i] + '"') > -1) {
                        result = true;
                    }
                }
            }

            return result;
        },
        setOauthUser: function (authData) {
            if (authData.sessionId !== undefined) {
                localStorage.setItem(AUTH_CONSTANTS.sessionId, authData.sessionId);
            }
            sessionStorage.setItem(AUTH_CONSTANTS.auth, JSON.stringify(authData));
        },
        getOauthUser: function () {
            let oauthUser = sessionStorage.getItem(AUTH_CONSTANTS.auth);

            return JSON.parse(oauthUser);
        },
        isAuth: function () {
            let oauthUser = this.getOauthUser();
            console.log(oauthUser);
            return (oauthUser !== null && oauthUser !== undefined && oauthUser.id > 0);
        },
        removeAuth: function () {
            sessionStorage.removeItem(AUTH_CONSTANTS.auth);
            sessionStorage.removeItem(AUTH_CONSTANTS.authorities);
        },
        getSessionId: function () {
            return localStorage.getItem(AUTH_CONSTANTS.sessionId);
        },
        setAuthorities: function (authorities) {
            sessionStorage.setItem(AUTH_CONSTANTS.authorities, JSON.stringify(authorities));
        }
    },
    watch: {}
}