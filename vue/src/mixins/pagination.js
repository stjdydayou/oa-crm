export default {
    data() {
        return {
            pageSize: 10,
            pageCurrentPage: 1,
            pageLayout: "total, sizes, prev, pager, next",
            pageSizes: [10, 30, 50, 100],
            pageTotalCount: 0
        }
    },
    methods: {
        pageHandleSizeChange(limit) {
            this.pageSize = limit;
        },
        pageHandleCurrentChange(currentPage) {
            this.pageCurrentPage = currentPage;
        }
    },
    watch: {}
}