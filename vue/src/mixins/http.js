import bus from '@/common/eventbus';
import {API_HOSH, AUTH_CONSTANTS} from '@/CONSTANTS';
import axios from "axios/index";

export default {
    data() {
        return {}
    },
    computed: {},
    beforeCreate() {

    },
    created() {
    },
    mounted() {

    },
    destroyed() {

    },
    methods: {
        httpPostRequest: function (url, data, callback, errorback) {
            let sessionId = localStorage.getItem(AUTH_CONSTANTS.sessionId);

            let instance = axios.post(url, data, {
                baseURL: API_HOSH,
                timeout: 10000,
                headers: {'x-session-id-header': (sessionId !== undefined && sessionId !== null) ? sessionId : ""}
            });
            instance.then(response => {
                console.log(response);
                let result = response.data;

                if (result.code === 'SUCCESS') {
                    callback(result.data);
                } else {
                    bus.$emit("message", "error", result.message);
                    if (errorback !== undefined) {
                        errorback(result.code, result.message);
                    }
                }
            }).catch(function (error) {
                bus.$emit("message", "warning", "连接到服务器失败，请稍后再试(" + error.message + ")");
                if (errorback !== undefined) {
                    errorback("NETWORK_ERROR", error.message);
                }
            })
        }
    }
}
