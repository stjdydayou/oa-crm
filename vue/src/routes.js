import NotFound from '@/views/404.vue'
import Forbidden from '@/views/403.vue'
import Login from '@/views/login.vue'
import Welcome from '@/views/welcome.vue'
import Main from '@/views/main.vue'


import oauthResource from '@/views/oauth/resource.vue'
import oauthRole from '@/views/oauth/role.vue'
import oauthUser from '@/views/oauth/user.vue'


let routes = [
    {path: '/welcome', component: Welcome, name: '欢迎', hidden: true, meta: {}},
    {path: '/login', component: Login, name: '登录', hidden: true, meta: {}},
    {path: '/404', component: NotFound, name: '404', hidden: true, meta: {}},
    {path: '/403', component: Forbidden, name: '403', hidden: true, meta: {}},
    {path: '*', hidden: true, redirect: {path: '/404'}, meta: {}},
    {
        path: '/main',
        component: Main,
        name: '系统设置',
        iconCls: 'el-icon-setting',//图标样式class
        roles: ['oauth/user/find', 'oauth/role/find', 'oauth/resource/find'],
        children: [
            {path: '/main/oauth/user', component: oauthUser, name: '用户管理', roles: ['oauth/user/find'], meta: {requireAuth: true}},
            {path: '/main/oauth/role', component: oauthRole, name: '角色管理', roles: ['oauth/role/find'], meta: {requireAuth: true}},
            {path: '/main/oauth/resource', component: oauthResource, name: '权限资源', roles: ['oauth/resource/find'], meta: {requireAuth: true}},
        ]
    }
];

export default routes;