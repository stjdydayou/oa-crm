package net.tyw.web.params;

/**
 * @author jtoms
 */
public class ListUserParam {
    private int currentPage = 1;

    private int limit = 15;

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }
}
