package net.tyw.web.servlet;


import net.tyw.common.utils.ValidateUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;

/**
 * @author jtoms
 */
public class ServletUtil {
    /**
     * <p>
     * Web 服务器反向代理中用于存放客户端原始 IP 地址的 Http header 名字，若新增其他的需要修改其中的值。
     * </p>
     */
    private final static String[] PROXY_REMOTE_IP_ADDRESS = {"X-Forwarded-For", "X-Real-IP"};

    /**
     * <p>
     * 获取请求的客户端的 IP 地址。若应用服务器前端配有反向代理的 Web 服务器， 需要在 Web 服务器中将客户端原始请求的 IP 地址加入到
     * HTTP header 中。 详见 {@link ServletUtil#PROXY_REMOTE_IP_ADDRESS}
     * </p>
     *
     * @return
     */
    public static String getRemoteIp() {
        HttpServletRequest request = ServletContext.getRequest();
        for (int i = 0; i < PROXY_REMOTE_IP_ADDRESS.length; i++) {
            String ip = request.getHeader(PROXY_REMOTE_IP_ADDRESS[i]);
            if (!ValidateUtil.isBlank(ip)) {
                return getRemoteIpFromForward(ip);
            }
        }
        return request.getRemoteHost();
    }

    /**
     * <p>
     * 从 HTTP Header 的 X-Forward-IP 头中截取客户端连接 IP 地址。如果经过多次反向代理， 在 X-Forward-IP
     * 中获得的是以“,&lt;SP&gt;”分隔 IP 地址链，第一段为客户端 IP 地址。
     * </p>
     *
     * @param xforwardIp
     * @return
     */
    private static String getRemoteIpFromForward(String xforwardIp) {
        int commaOffset = xforwardIp.indexOf(',');
        if (commaOffset < 0) {
            return xforwardIp;
        }
        return xforwardIp.substring(0, commaOffset);
    }

    /**
     * Created on 2017年2月17日
     * <p>Discription:[判断是否是IP地址]</p>
     *
     * @param text
     * @return
     * @author:[Walden]
     * @update:[日期YYYY-MM-DD] [更改人姓名]
     */
    public static boolean isIpAddress(String text) {
        if (ValidateUtil.isBlank(text)) {
            return false;
        }
        if (!text.matches("\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}")) {
            return false;
        }
        String[] ipAddressNode = text.split("\\.");
        for (String in : ipAddressNode) {
            int inInt = Integer.parseInt(in);
            if (inInt < 0 || inInt > 255) {
                return false;
            }
        }
        return true;
    }

    /**
     * Created on 2017年2月23日
     * <p>Discription:[获取二级域名paguwy.com]</p>
     *
     * @return
     * @author:[Walden]
     * @update:[日期YYYY-MM-DD] [更改人姓名]
     */
    public static String getSecondLevelDomain() {
        String host = ServletContext.getRequest().getServerName();
        if (ValidateUtil.isBlank(host)) {
            return "";
        }
        if (isIpAddress(host)) {
            return host;
        }
        String[] hostSplit = host.split("\\.");
        if (hostSplit.length == 1) {
            return host;
        } else {
            return hostSplit[hostSplit.length - 2] + "." + hostSplit[hostSplit.length - 1];
        }
    }

    /**
     * url encode
     *
     * @param src
     * @return
     */
    public static String urlEncode(String src) {
        try {
            src = java.net.URLEncoder.encode(src, "UTF-8");
            return src;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return src;
    }

    /**
     * url decoder
     *
     * @param src
     * @return
     */
    public static String urlDecoder(String src) {
        try {
            src = java.net.URLDecoder.decode(src, "UTF-8");
            return src;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return src;

    }

    public static String getBasePath() {
        HttpServletRequest request = ServletContext.getRequest();
        String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
        return basePath;
    }


    /**
     * 获取编码字符集
     *
     * @return String
     */
    public static String getCharacterEncoding() {
        HttpServletRequest request = ServletContext.getRequest();
        HttpServletResponse response = ServletContext.getResponse();
        if (null == request || null == response) {
            return "UTF-8";
        }
        String enc = request.getCharacterEncoding();
        if (null == enc || "".equals(enc)) {
            enc = response.getCharacterEncoding();
        }
        if (null == enc || "".equals(enc)) {
            enc = "UTF-8";
        }
        return enc;
    }
}
