package net.tyw.common;

public class JSONResult {

    private String code = "SUCCESS";

    private String message = "";

    private Object data;

    public static JSONResult instance() {
        return new JSONResult();
    }
    public static JSONResult instance(String code) {
        JSONResult json = new JSONResult();
        json.setCode(code);
        return json;
    }

    public static JSONResult instance(String code, String message) {
        JSONResult json = new JSONResult();
        json.setCode(code);
        json.setMessage(message);
        return json;
    }

    public String getMessage() {
        return message;
    }

    public JSONResult setMessage(String message) {
        this.message = message;
        return this;
    }

    public Object getData() {
        return data;
    }

    public JSONResult setData(Object data) {
        this.data = data;
        return this;
    }

    public String getCode() {
        return code == null ? null : code.toUpperCase();
    }

    public JSONResult setCode(String code) {
        this.code = code == null ? null : code.toUpperCase();
        return this;
    }
}
