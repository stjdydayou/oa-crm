package net.tyw.common.exception;

public class NotSufficientFundsException extends Exception {

    private static final long serialVersionUID = -6599531762911251584L;

    public NotSufficientFundsException() {
    }

    public NotSufficientFundsException(String message) {
        super(message);
    }

    public NotSufficientFundsException(Throwable cause) {
        super(cause);
    }

    public NotSufficientFundsException(String message, Throwable cause) {
        super(message, cause);
    }
}
