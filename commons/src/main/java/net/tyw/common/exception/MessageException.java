package net.tyw.common.exception;

public class MessageException extends Exception {

    private static final long serialVersionUID = -917135311064919387L;

    public MessageException(String message) {
        super(message);
    }
}
