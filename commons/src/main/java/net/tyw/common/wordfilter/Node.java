package net.tyw.common.wordfilter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author jtoms
 */
public class Node implements Serializable {

    private static final long serialVersionUID = 7243918706984177537L;

    private Map<String, Node> children = new HashMap<>(0);

    private boolean isEnd = false;

    private int level = 0;

    public Node addChar(char c) {
        String cStr = String.valueOf(c);
        Node node = this.children.get(cStr);
        if (node == null) {
            node = new Node();
            this.children.put(cStr, node);
        }
        return node;
    }

    public Node findChar(char c) {
        String cStr = String.valueOf(c);
        return this.children.get(cStr);
    }

    public boolean isEnd() {
        return this.isEnd;
    }

    public void setEnd(boolean isEnd) {
        this.isEnd = isEnd;
    }

    public int getLevel() {
        return this.level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public Map<String, Node> getChildren() {
        return children;
    }

    public void setChildren(Map<String, Node> children) {
        this.children = children;
    }
}
