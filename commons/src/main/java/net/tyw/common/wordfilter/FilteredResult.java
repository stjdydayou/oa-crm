package net.tyw.common.wordfilter;

import java.io.Serializable;

/**
 * @author jtoms
 */
public class FilteredResult implements Serializable {
    private static final long serialVersionUID = -8311690485198845005L;

    /**
     * 敏感词等级
     */
    private Integer level;
    
    /**
     * 被替换过的类容
     */
    private String filteredContent;
    
    /**
     * 敏感词
     */
    private String badWords;
    
    /**
     * 原始类容
     */
    private String originalContent;

    public String getBadWords() {
        return this.badWords;
    }

    public void setBadWords(String badWords) {
        this.badWords = badWords;
    }

    public FilteredResult() {
    }

    public FilteredResult(String originalContent, String filteredContent, Integer level, String badWords) {
        this.originalContent = originalContent;
        this.filteredContent = filteredContent;
        this.level = level;
        this.badWords = badWords;
    }

    public Integer getLevel() {
        return this.level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getFilteredContent() {
        return this.filteredContent;
    }

    public void setFilteredContent(String filteredContent) {
        this.filteredContent = filteredContent;
    }

    public String getOriginalContent() {
        return this.originalContent;
    }

    public void setOriginalContent(String originalContent) {
        this.originalContent = originalContent;
    }
}
