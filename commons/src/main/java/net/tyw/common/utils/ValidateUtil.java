package net.tyw.common.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author jtoms
 */
public class ValidateUtil {

    public static boolean isBlank(CharSequence cs) {
        return StringUtils.isBlank(cs);
    }

    public static boolean isNoneBlank(CharSequence cs) {
        return StringUtils.isNoneBlank(cs);
    }


    public static boolean isEmail(String value) {
        return validExpression(value, "^([_A-Za-z0-9-])+@(([A-Za-z0-9-])+\\.)+([a-zA-Z0-9]{2,4})+");
    }


    public static boolean isMobile(String value) {
        return validExpression(value, "^1[3|4|5|6|7|8][0-9]{9}");
    }

    public static boolean validExpression(String value, String expression) {
        return value != null && value.matches(expression);
    }

    public static boolean isUrl(String url) {
        if (StringUtils.isBlank(url)) {
            return false;
        } else {
            String regEx = "^((https?|ftp|mms):\\/\\/)?([A-z0-9]+[_\\-]?[A-z0-9]+\\.)*[A-z0-9]+\\-?[A-z0-9]+\\.[A-z]{2,}(\\/.*)*\\/?";
            Pattern p = Pattern.compile(regEx);
            Matcher matcher = p.matcher(url);
            return matcher.matches();
        }
    }
}
